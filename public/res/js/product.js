const products = [
    { name: "Apple", imageUrl: "apple.jpg" ,description: "Fresh and juicy apples from the orchard."},
    { name: "Banana", imageUrl: "banana.jpg",description: "Pure yellow goodness." },
    { name: "Orange", imageUrl: "orange.jpg" ,description: "Orange you glad we have these great oranges?"},
    { name: "Grapes", imageUrl: "grapes.jpg" ,description: "I won't make a joke here, for legal reasons."},
    { name: "Pineapple", imageUrl: "pineapple.jpg" ,description: "Useful for blowing up baloons."},
    { name: "Watermelon", imageUrl: "watermelon.jpg" ,description: "Regular melons with water in them."},
    { name: "Kiwi", imageUrl: "kiwi.jpg" ,description: "Hairiest fruit in the world."},
    { name: "Peach", imageUrl: "peach.jpg" ,description: "Your peach is in another castle."},
    { name: "Strawberry", imageUrl: "strawberry.jpg" ,description: "I wonder where the straw is."},
    { name: "Mango", imageUrl: "mango.jpg" ,description: "\"My mango is to blow up and then act like i know nobody\" -RiffRaff"}
];

// Function to render products
function renderProduct(products) {
    const params = new URLSearchParams(window.location.search);
    var fruit = params.get("fruit");
    const container = document.querySelector(".pcontainer");
    //this is dumb but whatever
    var index = products.findIndex(item => item.name === fruit);
    var obj = products[index];
        const productHTML = `
        <div class="product-details">
            <h2>${obj.name}</h2>
            <img src="./../img/${obj.imageUrl}" alt="${obj.name}">
            <p class="description">${obj.description}</p>
            <div class="rating">
                <span class="stars">&#9733;&#9733;&#9733;&#9733;&#9734;</span>
                <span class="reviews">(56 Reviews)</span>
            </div>
            <p class="price">$2.99 per kg</p>
            <button class="buy-button">Buy Now</button>
        </div>
        <div class="reviews">
            <h3>Customer Reviews</h3>
            <div class="review-container">
                <div class="review">
                    <div class="rating">
                        <span class="stars">&#9733;&#9733;&#9733;&#9734;&#9734;</span>
                    </div>
                    <p class="comment">Ooh-ooh-ahh-ahh!</p>
                    <img src="../img/monkey1.jpg" alt="Monkey Image" class="review-image">
                    <p class="author">- Coco</p>
                </div>
                <div class="review">
                    <div class="rating">
                        <span class="stars">&#9733;&#9733;&#9733;&#9734;&#9734;</span>
                    </div>
                    <p class="comment">Eek-eek-eek!</p>
                    <img src="../img/monkey2.jpg" alt="Monkey Image" class="review-image">
                    <p class="author">- Banjo</p>
                </div>
                <div class="review">
                    <div class="rating">
                        <span class="stars">&#9733;&#9733;&#9733;&#9733;&#9733;</span>
                    </div>
                    <p class="comment">Ah-ahh!</p>
                    <img src="../img/monkey3.jpg" alt="Monkey Image" class="review-image">
                    <p class="author">- Goober</p>
                </div>
                <div class="review">
                    <div class="rating">
                        <span class="stars">&#9733;&#9733;&#9734;&#9734;&#9734;</span>
                    </div>
                    <p class="comment">Ah-ah-ahh!</p>
                    <img src="../img/monkey4.jpg" alt="Monkey Image" class="review-image">
                    <p class="author">- Mocha</p>
                </div>
                <div class="review">
                    <div class="rating">
                        <span class="stars">&#9733;&#9733;&#9733;&#9733;&#9734;</span>
                    </div>
                    <p class="comment">Ooh-ooh!</p>
                    <img src="../img/monkey5.jpg" alt="Monkey Image" class="review-image">
                    <p class="author">- Bongo</p>
                </div>
            </div>
        </div>

                   `;
        container.innerHTML = productHTML;
}

// Initial rendering of products
renderProduct(products);
