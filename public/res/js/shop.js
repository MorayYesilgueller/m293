const products = [
    { name: "Apple", imageUrl: "apple.jpg" ,description: "Fresh and juicy apples from the orchard."},
    { name: "Banana", imageUrl: "banana.jpg",description: "Pure yellow goodness." },
    { name: "Orange", imageUrl: "orange.jpg" ,description: "Orange you glad we have these great oranges?"},
    { name: "Grapes", imageUrl: "grapes.jpg" ,description: "I won't make a joke here, for legal reasons."},
    { name: "Pineapple", imageUrl: "pineapple.jpg" ,description: "Useful for blowing up baloons."},
    { name: "Watermelon", imageUrl: "watermelon.jpg" ,description: "Regular melons with water in them."},
    { name: "Kiwi", imageUrl: "kiwi.jpg" ,description: "Hairiest fruit in the world."},
    { name: "Peach", imageUrl: "peach.jpg" ,description: "Your peach is in another castle."},
    { name: "Strawberry", imageUrl: "strawberry.jpg" ,description: "I wonder where the straw is."},
    { name: "Mango", imageUrl: "mango.jpg" ,description: "\"My mango is to blow up and then act like i know nobody\" -RiffRaff"}
];

// Function to render products
function renderProducts(products) {
    const container = document.querySelector(".container");
    container.innerHTML = ""; // brug

    products.forEach(product => {
        const productHTML = `
            <section class="product">
                <a href="product.html?fruit=${product.name}" class="product-link">
                    <img src="./../img/${product.imageUrl}" alt="${product.name}">
                    <h2>${product.name}</h2>
                    <p class="description">${product.description}</p>
                    <div class="rating">
                        <span class="stars">&#9733;&#9733;&#9733;&#9733;&#9734;</span>
                        <span class="previews">(56 Reviews)</span>
                    </div>
                </a>
            </section>
        `;
        container.innerHTML += productHTML;
    });
}

// Function to filter products based on search input
function filterProducts(searchTerm) {
    const filteredProducts = products.filter(product =>
        product.name.toLowerCase().includes(searchTerm.toLowerCase())
    );
    renderProducts(filteredProducts);
}

// Initial rendering of products
renderProducts(products);

// Event listener for search input
document.getElementById("search-input").addEventListener("input", function() {
    const searchTerm = this.value.trim(); // Get search term and remove leading/trailing whitespace
    filterProducts(searchTerm);
});